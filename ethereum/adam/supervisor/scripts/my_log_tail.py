#!/usr/bin/env python
import time
import subprocess
import select
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s:', level=logging.INFO)

# Let things start before we reporcess the restart
time.sleep(10)
filename = "/var/tmp/ethproxy.err.log"

f = subprocess.Popen(['tail','-F',filename],\
        stdout=subprocess.PIPE,stderr=subprocess.PIPE)
p = select.poll()
p.register(f.stdout)

logging.info('Starting process to tail %s', filename)
while True:
    if p.poll(1):
        logline= f.stdout.readline().rstrip('\r\n')
        time.sleep(1)
        if "Work from 'rx560' REJECTED" in logline:
           # restart stupidvisord
           logging.info('Restarting ethproxy due to work rejected in log') 
           process = subprocess.call(['supervisorctl', '-c', '/home/adam/work/virtualenv/supervisord/ethermine.conf', 'restart', 'ethproxy', 'ethminer'], shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
           logging.info('Restart complete: %s', str(process) ) 

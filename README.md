# Intro 

This repo is a place for sharing mining configs, tips, & tricks.  Eventually this README.md file will become more of a pointer, 
with specifics & settings within the appropriate sub-directory.  For example, notes from Adam should be in the Adam direcotry of 
the appropriate altcoin. For now this the main documentation source.

# Ethereum 

## Mining pool notes

Adam has spent several hours exploring ethermine.org & nanopool.  The issue is knowing which miner to use with which pool.  Some
pools like ethermine prefer Genoils Miner.  While Genoils Miner has similar flags to ethminer, they are different miners. The 
claymore miner is a popular alternative, but it's a fee based miner that works by submitting hash rates to a specific wallet 
owned by the claymore author. (Honestly I'd like to spend some time figuring out how to overwrite the wallet address and publish
that information to prevent the author from taking money from others.)

### Dwarfpool 

Dwarfpool is what both Adam & Jack are using. It has a relatively high fee of 2%, but offers anonyonmous logins is easy to setup.
There are questionable downtime's and reddit seems to hate dwarfpool.  Perhaps its time to moooove along?

### Ethermine

Ethermine is another pool which supports anonomous mining.  While Adam attempted to connect to the ethermin pool, he was 
unsuccessful and gave up after several hours. He thinks it's worth a revisit, but before doing so keep in mind the TCP connection
type of either http or stratum when connecting with your miner. (See eth-proxy notes in software section below)

## Hardware Notes

Watch youtube videos for using MSI Afterburner to tweek your AMD card or EVGA Precision X to tweek your Nvidia card.  Also keep
in mind card families when runing multiminer systems.  You may not want to mix old & new AMD cards on the same system if the new
driver won't work on the old card, forcing a performance hit from not running the optiumm driver.

### AMD RX570

After moddifying the bios, this card is capable of ~ 25 MH/s at 138 watts.  It's believed further work with power settings can
bring this card a few watts lower on power consumption. 


### AMD RX 560

Out of the box, the RX560 does ~ 10 MH/s and is rated by AMD to pull 80 Watts. (Power usage needs to be measured with a killowatt
but this will happen soon.)  Supposedly people report up to 12-13 MH/s with this card.  To reach these speeds, it needs to be
overclocked & undervolted.  Adam has a windows host he can test changes with, when he gets his 'round-2-it' back.

### Nvidia GTX1080TI

This card is a bruiser ... don't get in it's way.

## Adam's Ethereum setup 

### Overview
The OS used by Adam is Ubuntu 16.04.2, which allows a mix of using both github source or precompiled apt packages. AMD has a 
compiled video card driver.  Adam uses the AMDGPU-PRO driver for linux, version 17.10.2.  

In addition to running a full Ethereum node using geth, Adam uses a combination of ethminer and eth-proxy for mining.  The 
supposed advanatages of using eth-proxy with a miner is stratum connections are faster as miners talk http & the proxy can connect
directly with the stratum servers.

Python's supervisor is used to manage running processes.  Supervisor is repsonsible for starting & stopping all mining processes,
as well as restarting the proxy 

### Motherboard BIOS settings

* Generation 2 in PCI timings (or you'll hate life and by dozens of risers like Adam)

### Video Card Bios Mods.
TODO: Document windows setup

### Linux Video Card driver

**AMD**:  Download the driver from AMDs website. To onstall the driver:

1. Update the OS, `sudo apt update && sudo apt upgrade`
     * [reference doc](http://support.amd.com/en-us/kb-articles/Pages/AMDGPU-PRO-Install.aspx)
2. Download the driver and extract using `tar -Jxvf amdgpu-pro-17.30-NNNNNN.tar.xz`
3.  Run the install script. `amdgpu-pro-install –y`
4. **VERY IMPORTANT** Make sure to grant permissions to the video group 
     * otherwise you'll need to sudo everything. 
     * `sudo usermod -a -G video $LOGNAME `.  
5. reboot
6. install rocm library
     * `sudo apt install -y rocm-amdgpu-pro`
     * update /etc/profile `echo 'export LLVM_BIN=/opt/amdgpu-pro/bin' | sudo tee /etc/profile.d/amdgpu-pro.sh`
7. Modify grub to expose rocm library to GPUs (~30% hash rate increase)
     * [reference doc](https://codesport.io/mining-tutorials/amd-blockchain-compute-driver-for-linux/)
     * `sudo vi /etc/default/grub` to set 2MB fragments for Ellesmere
     * GRUB_CMDLINE_LINUX="amdgpu.vm_fragment_size=9" in vim session 
     * sudo update-grub 
     * sudo reboot



**NVIDEA**: TODO


### OPENCL install notes (linux)

* TODO

### CUDA install notes (linux)

* TODO

### Mining Software
1. [ethminer](https://github.com/ethereum/go-ethereum/wiki/Installation-Instructions-for-Ubuntu)  is installed from a Ubuntu 
   package and is what mines ethereum.  It has AMD support compiled into it.
2. [geth](https://github.com/ethereum/go-ethereum/wiki/Installation-Instructions-for-Ubuntu) is installed from a Ubuntu package 
   and is used for managing the block chain.
3. [eth-proxy](https://github.com/Atrides/eth-proxy) is able to convert http traffic to stratum traffic. The local miner is able 
   to send http traffic to local host and let the proxy convert it to stratum when it crosses the internet.  eth-proxy is a python 
   script that is ran within a virtualenv environment.  
4. [supervisor](http://supervisord.org/installing.html) is used to start & stop mining processes.  It is another python script
   installed using "pip install" and installed into a virtualenv environment.
5. my_log_tail.py is a simple script that tails the output of the eth-proxy process and restarts both eth-proxy & ethminer if
   eth-proxy logs a work submission error.

### Files

"ethereum/adam/eth-proxy" contains the configuration file for eth-proxy.  If copying this file, make sure to change the wallet
address or you'll end up mining for Adam. Just replace the github  eth-proxy.conf file with this one and you'll be set. 

"ethereum/adam/supervisor/conf/ethermine.conf" Is the config file used by supervisor.  It logs into /var/tmp, so keep in mind
partition space if using it.  Also note the paths to binaries might be different on your system so change accordingly. 

"ethereum/adam/supervisor/scripts/my_log_tail.py" as mentioned earlier, this looks for  "Work from 'rx560' REJECTED" in the log
file.  This string will vary based on what you tell what url ethminer to connect as in ethermine.conf and may need updating.  The
script is not perfect. Adam lost ~ 36 hours of mining because eth-proxy rejected connections on localhost after a restart due to
"REJECTED" log entries.  To fix a 2nd log tail script should watch the output of ethminer and trigger a ethminer restart if 
connections are refused.

### Installation

As everthing is mostly python based, I created a virtualenv environment to contain things.  Here is the output of 'pip freeze' to
show which dependancies are included in the virtualenv.

```
(supervisord) adam@ubuntu:~/work/public/altcoin-settings$ pip freeze
meld3==1.0.2
pkg-resources==0.0.0
superlance==1.0.0
supervisor==3.3.2
Twisted==16.1.1
zope.interface==4.1.3
```

After entering the virtualenv environment, Dependencies were installed using 'pip install -r requirements.txt'.  Where 
requirements.txt was included from the github repo the project was downloaded from.  Some of these requirements are for 
supervisor others are for eth-proxy as everything was installed in the same virutalenv environment.

### Startup

I find it best to start a tmux session so I can have long running commands in related panes. This also allows one to 
attach/detach to a mining instance.

First one will want to start geth to download the current blockchain by typing ```geth console```.  Wait a few minutes and you'll 
see geth peer with other instances and download the block chain.  You'll want a second pane for geth, where one types 
```geth attach```.  This is useful for setting your wallet address to see how many stacks of fat ethereum coin you have.

One will want to start the supervisor process using ```supervisord -c /path/to/my/ethermine.conf```. Once supervisor is running, 
one will use 'supervisorctl' for process restarts and what not.  When supervisord starts, it also starts eth-proxy, ethminer, &
the tail process to tail eth-proxy logs. Supervisor is repsonsbile for rolling log files, but make sure a full filesystem won't
be a issue.

Other panes Adam opens are tail -f processes on the eth-proxy & ethminer logs in /var/tmp.  Total open panes are two for geth, 
three for tailing logs, one running htop, and one to run commands like ```sensors``` within to see temperature readings.  In 
total, there are 7 panes left open within a single tmux window.


## Charlie's Ethereum setup 

How long before Charlie pulls out his monster 8 card rig and blows us poor scrub
miners off the mountain? Until then we keep scrubbing away.

## Jacks's Ethereum setup 

Using tons of TNT and hashing the ethereum mountainside at 43 MH/s.  Claims he'll "forget" to reset the wallet
address from Adam's eth-proxy settings for a few days.  Its true that he's no hardened desparado, but
he's no greenhorn either. 

# Zcash (ZEC)

## place holder for zcash coins which can be mined with cards


# Monero 

## another Proof of work alt coin

